package com.imdb.pages;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.commerceiq.page.utilities.BasePageMethods;

public class MovieDetailsPage extends BasePageMethods {

	@FindBy(xpath = "//div[@class='title_wrapper']/h1")
	private WebElement movieTitle;

	@FindBy(xpath = "//span[@itemprop='ratingValue']")
	private WebElement movieRating;

	@FindBy(xpath = "//span[@id='titleYear']/a")
	private WebElement movieYear;
	
	public MovieDetailsPage() {
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 5), this);
	}

	
	public HashMap<String, String> getMovieDetail() {
		HashMap<String, String> movieDetails = new HashMap<String, String>();
		try {
			String title = waitAndGetText(movieTitle,10);
			String year = getText(movieYear);
			String rating = getText(movieRating);
			
			movieDetails.put("title",title);
			movieDetails.put("year",year);
			movieDetails.put("rating",rating);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		} 
		return movieDetails;
	}
}
