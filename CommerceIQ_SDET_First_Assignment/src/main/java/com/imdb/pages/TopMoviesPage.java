package com.imdb.pages;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.commerceiq.page.utilities.BasePageMethods;


public class TopMoviesPage extends BasePageMethods {

	@FindBy(xpath = "//tbody[@class='lister-list']/tr/td[@class='titleColumn']/a")
	private List<WebElement> imdbTopMovieTitles;

	@FindBy(xpath = "//tbody[@class='lister-list']/tr/td[@class='ratingColumn imdbRating']/strong")
	private List<WebElement> imdbTopMovieRatings;

	@FindBy(xpath = "//tbody[@class='lister-list']/tr/td[@class='titleColumn']/span")
	private List<WebElement> imdbTopMovieYears;

	public TopMoviesPage() {
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 5), this);
	}

	public boolean getTopMovieDetails(int numberOfMovies) {
		Connection conn = null;
		try {
			
			String dbPath = "jdbc:sqlite:" + "src/test/resources/sqliteDB/imdbMovies.db";
			conn = DriverManager.getConnection(dbPath);

			String createsqlStatement = "CREATE TABLE IF NOT EXISTS topMovieDetails (id integer PRIMARY KEY, "
					+ "listTitle text, actualTitle text, "
					+ "listYear integer, actualYear integer,"
					+ "listRating text, actualRating text);";

			Statement stmt = conn.createStatement();
			stmt.execute(createsqlStatement);

			String insertsqlStatement = "INSERT INTO topMovieDetails(id,listTitle,listYear,listRating) VALUES(?,?,?,?)";
			String updatesqlStatement = "UPDATE topMovieDetails SET listTitle = ?, listYear = ?, listRating = ? WHERE id = ?";

			for (int id = 1; id <= numberOfMovies; id++) {
				String title = getText(imdbTopMovieTitles.get(id-1));
				String yearString = getText(imdbTopMovieYears.get(id-1));
				int year = Integer.parseInt(yearString.substring(1, yearString.length() - 1));
				String rating = getText(imdbTopMovieRatings.get(id-1));

				try {
					PreparedStatement pstmt = conn.prepareStatement(insertsqlStatement);
					pstmt.setInt(1, id);
					pstmt.setString(2, title);
					pstmt.setInt(3, year);
					pstmt.setString(4, rating);
					pstmt.executeUpdate();
					
					System.out.println(id+") data inserted");
				} catch(SQLException inserException) {
					PreparedStatement pstmt = conn.prepareStatement(updatesqlStatement);
					pstmt.setString(1, title);
					pstmt.setInt(2, year);
					pstmt.setString(3, rating);
					pstmt.setInt(4, id);
					pstmt.executeUpdate();
					
					System.out.println(id+") data got updated");
				}
				
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				System.out.println(ex.getMessage());
			}
		}
		return true;
	}
	
	public boolean clickMovieByRank(int rank) {
		imdbTopMovieTitles.get(rank-1).click();
		return true;
	}
	
}
