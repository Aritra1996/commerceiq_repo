package com.commerceiq.drivers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.commerceiq.page.utilities.ExceptionType;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverClass {

	public DriverClass() {
		super();
	}

	protected static WebDriver driver;
	private Properties prop;
	private static Actions actions;
	protected static WebDriverWait wait;
	
	protected void setDriver() throws Exception {
		prop = new Properties();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(System.getProperty("user.dir") + "//src//test//resources//config.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if (fis == null) {
			throw new ExceptionType("Property File not present");
		} else {
			prop.load(fis);
			String browser = prop.getProperty("browser");
			if(browser==null) {
				throw new ExceptionType("Browser Name not present");
			} else {
				if (browser.equalsIgnoreCase("chrome")) {
					WebDriverManager.chromedriver().setup();
					driver = new ChromeDriver();
				} else if (browser.equalsIgnoreCase("firefox")) {
					WebDriverManager.firefoxdriver().setup();
					driver = new FirefoxDriver();
				} else {
					throw new ExceptionType("Browser not supported");
				}
				driver.manage().deleteAllCookies();
				driver.manage().window().maximize();
				driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			}
		}
		actions = new Actions(driver);
	}
	
	protected boolean openURL(String url) throws IOException {
		driver.get(url);
		return true;
	}

	protected void quitDriver() {
		driver.close();
		driver.quit();
	}
	
	protected void hoverOverElement(WebElement elem) {
		actions.moveToElement(elem).perform();
	}
}
