package com.commerceiq.page.utilities;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.commerceiq.drivers.DriverClass;

public class BasePageMethods extends DriverClass {

	private static JavascriptExecutor executor;

	protected void sendKeys(WebElement elem, String keys) {
		elem.clear();
		elem.sendKeys(keys);
	}

	protected void click(WebElement elem) {
		elem.click();
	}

	protected void waitAndClick(WebElement elem, int seconds) {
		wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOf(elem));
		elem.click();
	}

	protected void clickUsingJS(WebElement elem) {
		executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", elem);
	}

	protected void waitAndClickUsingJS(WebElement elem, int seconds) {
		wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOf(elem));
		executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", elem);
	}

	protected String getText(WebElement elem) {
		return elem.getText();
	}

	protected String waitAndGetText(WebElement elem, int seconds) {
		wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOf(elem));
		return elem.getText();
	}

	protected boolean scrollToElement(WebElement elem)  {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elem);
		if (elem.isDisplayed())
			return true;
		else
			return false;
	}

	protected boolean scrollToElementUsingXpath(String elementXpath) {
		WebElement elem = driver.findElement(By.xpath(elementXpath));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elem);
		if (elem.isDisplayed())
			return true;
		else
			return false;
	}

	protected boolean scrollToElementUsingText(String text) {
		String xpath = "//*[contains(text(),'" + text + "')]";
		WebElement elem = driver.findElement(By.xpath(xpath));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elem);
		if (elem.isDisplayed())
			return true;
		else
			return false;
	}

	protected boolean findElement(WebElement elem) {
		if (elem.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}

	protected boolean waitAndFindElement(WebElement elem, int seconds) {
		wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOf(elem));
		return true;
	}
	
	protected boolean waitAndFindElement(List<WebElement> elems, int seconds) {
		wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOfAllElements(elems));
		return true;
	}
	
	protected boolean waitAndFindElementPresenceByXpath(String xpath, int seconds) {
		wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		return true;
	}
	
	protected boolean waitAndFindElementByXpath(String xpath, int seconds) {
		wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		return true;
	}
	
	protected boolean waitTillUrlContains(String fractionUrl, int seconds) {
		wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.urlContains(fractionUrl));
		System.out.println("Current url :- "+ driver.getCurrentUrl());
		return true;
	}
	
	public boolean goToPreviousPage() {
		driver.navigate().back();
		return true;
	}

}
