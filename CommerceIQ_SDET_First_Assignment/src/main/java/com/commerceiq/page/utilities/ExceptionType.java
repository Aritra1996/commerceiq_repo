package com.commerceiq.page.utilities;

public class ExceptionType extends Exception {


	/**
	 * 
	 */
	private static final long serialVersionUID = -2187270876612568875L;

	public ExceptionType(String s) {
		super(s);
	}
}
