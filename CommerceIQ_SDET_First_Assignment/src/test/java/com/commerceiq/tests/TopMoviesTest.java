package com.commerceiq.tests;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.commerceiq.test.utilities.BaseTestMethods;
import com.commerceiq.test.utilities.TestDataUtility;
import com.imdb.pages.MovieDetailsPage;
import com.imdb.pages.TopMoviesPage;

public class TopMoviesTest extends BaseTestMethods {

	private String className = this.getClass().getSimpleName();

	private static String testDataSheetPath = "src/test/resources/TestDatas/TopMoviesTestDataSheet.xlsx";
	private static String checkTopMoviesDetailsSheetName = "CheckTopMoviesDetailsSheet";

	@BeforeClass
	private void beforeClass() {
		try {
			setUpReport(className);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	private void afterClass() {
		destroyReport();
	}

	@BeforeMethod
	private void beforeMethod() {
		try {
			setDriver();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterMethod
	private void afterMethod() {
		quitDriver();
	}

	@DataProvider
	private Object[][] checkTopMoviesDetailsGetData() {
		Object object[][] = TestDataUtility.getTestData(testDataSheetPath, checkTopMoviesDetailsSheetName);
		return object;
	}

	@Test(dataProvider = "checkTopMoviesDetailsGetData")
	private void checkTopMoviesDetailsTest(String url, String numberOfMovies) throws InterruptedException, IOException {

		startTest("checkTopMoviesDetailsTest");

		TopMoviesPage topMoviesPage = new TopMoviesPage();

		try {

			openURL(url);
			logReportPass(className, "openURL", "Url: " + url + " opened");

			if (topMoviesPage.getTopMovieDetails(Integer.parseInt(numberOfMovies))) {
				logReportPass(className, "getTopMovieDetails",
						"Top " + numberOfMovies + " movie details inserted in DB");
			} else {
				logReportFail(className, "getTopMovieDetails",
						"Top " + numberOfMovies + " movie details not inserted in DB");
			}

			Connection conn = null;
			try {

				String dbPath = "jdbc:sqlite:" + "src/test/resources/sqliteDB/imdbMovies.db";
				conn = DriverManager.getConnection(dbPath);

				String selectsqlStatement = "SELECT listTitle, listYear, listRating FROM topMovieDetails WHERE id = ?";
				String insertsqlStatement = "INSERT INTO topMovieDetails(id,actualTitle,actualYear,actualRating) VALUES(?,?,?,?)";
				String updatesqlStatement = "UPDATE topMovieDetails SET actualTitle = ?, actualYear = ?, actualRating = ? WHERE id = ?";

				for (int id = 1; id <= Integer.parseInt(numberOfMovies); id++) {
					PreparedStatement pstmt = conn.prepareStatement(selectsqlStatement);
					pstmt.setInt(1, id);
					ResultSet rs = pstmt.executeQuery();

					String title = null, rating = null;
					int year = 0;
					while (rs.next()) {
						title = rs.getString("listTitle");
						year = rs.getInt("listYear");
						rating = rs.getString("listRating");
					}

					topMoviesPage.clickMovieByRank(id);
					MovieDetailsPage movieDetailsPage = new MovieDetailsPage();
					HashMap<String, String> movieDetails = movieDetailsPage.getMovieDetail();

					if (movieDetails.get("title").contains(title)) {
						logReportPass(className, "checkMovieDetails",
								"Movie "+id + ") Expected title : " + title + ", Actual title : " + movieDetails.get("title"));
					} else {
						logReportVerificationFail(className, "checkMovieDetails",
								"Movie "+id + ") Expected title : " + title + ", Actual title : " + movieDetails.get("title"));
					}

					if (year == Integer.parseInt(movieDetails.get("year"))) {
						logReportPass(className, "checkMovieDetails",
								"Movie "+id + ") Expected year : " + year + ", Actual year : " + movieDetails.get("year"));
					} else {
						logReportVerificationFail(className, "checkMovieDetails",
								"Movie "+id + ") Expected year : " + year + ", Actual year : " + movieDetails.get("year"));
					}

					if (rating.equalsIgnoreCase(movieDetails.get("rating"))) {
						logReportPass(className, "checkMovieDetails", "Movie "+id + ") Expected rating : " + rating
								+ ", Actual rating : " + movieDetails.get("rating"));
					} else {
						logReportVerificationFail(className, "checkMovieDetails", "Movie "+id + ") Expected rating : " + rating
								+ ", Actual rating : " + movieDetails.get("rating"));
					}

					try {
						pstmt = conn.prepareStatement(insertsqlStatement);
						pstmt.setInt(1, id);
						pstmt.setString(2, movieDetails.get("title"));
						pstmt.setInt(3, Integer.parseInt(movieDetails.get("year")));
						pstmt.setString(4, movieDetails.get("rating"));
						pstmt.executeUpdate();

						System.out.println("Movie "+id + ") data inserted");
						
					} catch (SQLException inserException) {
						
						pstmt = conn.prepareStatement(updatesqlStatement);
						pstmt.setString(1, title);
						pstmt.setInt(2, Integer.parseInt(movieDetails.get("year")));
						pstmt.setString(3, movieDetails.get("rating"));
						pstmt.setInt(4, id);
						pstmt.executeUpdate();

						System.out.println("Movie "+id + ") data got updated");
					}

					movieDetailsPage.goToPreviousPage();
				}

			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println(e.getMessage());
			} finally {
				try {
					if (conn != null) {
						conn.close();
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					System.out.println(ex.getMessage());
				}
			}

		} catch (Exception e) {
			logReportFail(className, "checkTopMoviesDetailsTest", e);
		} finally {
			endTest();
		}

	}
}
