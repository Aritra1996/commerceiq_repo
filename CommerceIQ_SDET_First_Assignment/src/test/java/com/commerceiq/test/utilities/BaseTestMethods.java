package com.commerceiq.test.utilities;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.commerceiq.drivers.DriverClass;
import com.commerceiq.page.utilities.ExceptionType;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BaseTestMethods extends DriverClass {

	private String startTimeString = null;
	private static ExtentReports report;
	private static ExtentTest test;
	private SoftAssert softAssert = new SoftAssert();

	public BaseTestMethods() {
		super();
	}

	protected String captureScreenShot(String testStep, String startTimeString) throws IOException {
		long currTime = System.currentTimeMillis();
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String currentTime = String.valueOf(currTime);
		String imagePath = "Reports" + "/" + startTimeString + "/" + "Screenshots/" + testStep + currentTime + ".png";
		FileUtils.copyFile(src, new File(imagePath));

		String imagePathReport = "Screenshots/" + testStep + currentTime + ".png";
		return imagePathReport;
	}

	protected void setUpReport(String className) {
		long starttime = System.currentTimeMillis();
		startTimeString = String.valueOf(starttime);
		report = new ExtentReports(
				"Reports/" + className + startTimeString + "/" + "ExtentReport.html");
	}
	
	protected void destroyReport() {
		report.flush();
	}
	
	protected void startTest(String testName) {
		test = report.startTest(testName);
	}
	
	protected void endTest() {
		report.endTest(test);
	}

	protected void logReportPass(String className, String stepName, String details) throws IOException {
		test.log(LogStatus.PASS, stepName, details);
		test.log(LogStatus.PASS, "Snapshot beside", test
				.addScreenCapture(captureScreenShot(stepName, className + startTimeString)));
	}
	
	protected void logReportFail(String className, String stepName, String details) throws IOException {
		new ExceptionType(details).printStackTrace();
		test.log(LogStatus.FAIL, stepName, details);
		test.log(LogStatus.FAIL, "Snapshot beside", test
				.addScreenCapture(captureScreenShot(stepName, className + startTimeString)));
		Assert.fail(details);
	}
	
	protected void logReportVerificationFail(String className, String stepName, String details) throws IOException {
		new ExceptionType(details).printStackTrace();
		test.log(LogStatus.FAIL, stepName, details);
		test.log(LogStatus.FAIL, "Snapshot beside", test
				.addScreenCapture(captureScreenShot(stepName, className + startTimeString)));
		softAssert.fail(details);
	}
	
	protected void logReportFail(String className, String stepName, Exception e) throws IOException {
		e.printStackTrace();
		test.log(LogStatus.FAIL, stepName, e);
		test.log(LogStatus.FAIL, "Snapshot beside", test
				.addScreenCapture(captureScreenShot(stepName, className + startTimeString)));
		Assert.fail(e.getMessage());
	}
	
	protected void logReportFail(String className, String stepName, String details, Exception e) throws IOException {
		e.printStackTrace();
		test.log(LogStatus.FAIL, stepName, details);
		test.log(LogStatus.FAIL, stepName, e);
		test.log(LogStatus.FAIL, "Snapshot beside", test
				.addScreenCapture(captureScreenShot(stepName, className + startTimeString)));
		Assert.fail(details);
	}
	
	protected void logReportInfo(String className, String stepName, String details) throws IOException {
		test.log(LogStatus.INFO, stepName, details);
		test.log(LogStatus.INFO, "Snapshot beside", test
				.addScreenCapture(captureScreenShot(stepName, className + startTimeString)));
	}
	
	protected void logReportWarn(String className, String stepName, String details) throws IOException {
		test.log(LogStatus.WARNING, stepName, details);
		test.log(LogStatus.WARNING, "Snapshot beside", test
				.addScreenCapture(captureScreenShot(stepName, className + startTimeString)));
	}

}
