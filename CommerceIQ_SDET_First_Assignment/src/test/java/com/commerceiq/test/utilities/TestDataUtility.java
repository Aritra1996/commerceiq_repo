package com.commerceiq.test.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;


public class TestDataUtility {

	private static Workbook workbook;
	private static Sheet sheet;

	public static Object[][] getTestData(String workbookPath, String sheetName) {
		FileInputStream file = null;
		try {
			file = new FileInputStream(workbookPath);
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			try {
				workbook = WorkbookFactory.create(file);
			} catch (EncryptedDocumentException e) {
				e.printStackTrace();
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
		
		sheet = workbook.getSheet(sheetName);
		Object[][] data = new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
		
		System.out.println("Number of Rows in sheet :- "+sheet.getLastRowNum());
		System.out.println("Number of columns in sheet :- "+sheet.getRow(0).getLastCellNum());
		
		for(int i=0; i<sheet.getLastRowNum(); i++) {
			for(int j=0; j<sheet.getRow(0).getLastCellNum(); j++) {
				sheet.getRow(i+1).getCell(j).setCellType(CellType.STRING);
				data[i][j] = sheet.getRow(i+1).getCell(j).toString();
			}			
		}
		
		return data;
	}
	
	public static String[][] getTestDataString(String workbookPath, String sheetName) {
		FileInputStream file = null;
		try {
			file = new FileInputStream(workbookPath);
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			try {
				workbook = WorkbookFactory.create(file);
			} catch (EncryptedDocumentException e) {
				e.printStackTrace();
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
		
		sheet = workbook.getSheet(sheetName);
		String[][] data = new String[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
		
		for(int i=0; i<sheet.getLastRowNum(); i++) {
			for(int j=0; j<sheet.getRow(i).getLastCellNum(); j++) {
				sheet.getRow(i+1).getCell(j).setCellType(CellType.STRING);
				data[i][j] = sheet.getRow(i+1).getCell(j).toString();
			}			
		}
		
		return data;
	}
}
